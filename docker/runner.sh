#!/bin/bash

# https://stackoverflow.com/questions/54534387/how-gitlab-runner-concurrency-works

# Requirement
# Docker to be installed

# It will work the same as the AWS EC2 runner. 

docker volume create gitlab-runner-config-sm-devops

docker run -d --name gitlab-runner-aws-sm-devops --restart always \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v gitlab-runner-config-sm-devops:/etc/gitlab-runner \
    gitlab/gitlab-runner:latest


docker volume create gitlab-runner-config-sm-devops-two

docker run -d --name gitlab-runner-aws-sm-devops-two --restart always \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v gitlab-runner-config-sm-devops-two:/etc/gitlab-runner \
    gitlab/gitlab-runner:latest


# Register our project into the container
# Run only the last cmd in Gitlab Runner installtion steps got from our project CI/CD Runner setting
gitlab-runner register --url http://gitlab-machine.dev/ --registration-token <token>

# Container Requirement
# Install aws cli
# Install unzip
# Install git

apt-get update
apt-get install nano
apt-get install unzip
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
./aws/install
aws --version
gitlab-runner register --url https://gitlab.com/ --registration-token GR1348941g-4P3KNrxTixw5FbCkSu
