#!/bin/bash

echo "Deploy script ready to start..."

echo "Compiling the code..."
echo "Compile complete."

echo "Running unit tests... This will take about 60 seconds."
sleep 60
echo "Code coverage is 90%

echo "Linting code... This will take about 10 seconds."
sleep 10
echo "No lint issues found.

echo "Deploying application..."
echo "Application successfully deployed."
