#!/bin/bash

echo "Lint test job ready to start..."

echo "Linting code... This will take about 10 seconds."
sleep 30
echo "No lint issues found."
